﻿using System;
using System.IO;

namespace AMiON.Helper
{
    public class AmiOnDataProvider
    {
        private static string _amiOnUrl = @"http://www.amion.com/cgi-bin/ocs?Lo={0}&Rpt=625tabs--&Day={1}&Month={2}&Year=2019";
        public static Stream GetAmionData(string amionLogin = "")
        {
            amionLogin = string.IsNullOrEmpty(amionLogin) ? "az1" : amionLogin;
            string amiOnUrl = string.Format(_amiOnUrl, amionLogin, DateTime.Now.Day, DateTime.Now.Month);
            return RestHelper.GetDataFromUrl(amiOnUrl) == null ? null : RestHelper.GetDataFromUrl(amiOnUrl).GetAwaiter().GetResult();
        }
    }
}
