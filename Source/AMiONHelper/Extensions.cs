﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Text;

namespace AMiON.Helper
{
    public static class Extensions
    {
        public static StringContent AsJson(this object o)
        => new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");

        public static T AsObject<T>(this object o) => JsonConvert.DeserializeObject<T>(o.ToString());

        public static JObject ParseJson(this object o) => JObject.Parse(o.ToString());
    }
}
