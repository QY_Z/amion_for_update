﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AMiON.Helper
{
    public static class ShiftsProcessor
    {
        public static int ShiftCreationProcessor(string token, AMiONShiftMappingModel objAMiONShiftMappingModel, List<string> selectedTeams = null, DataTable mappingFile = null)
        {
            int shiftCreationCount = 0;

            List<Team> teamsWhichNeedsTobeProcessedBasedOnSelectedDepartment = null;
            //Get data from AmiOn in Json
            List<AssignmentDetails> _assignments = Text2Json.JsonDataFromWebExcel(objAMiONShiftMappingModel.AmionLogin);

            if (string.IsNullOrEmpty(token))
            {
                //Get auth token
                token = Authentication.GetToken();
            }

            //Get teams data from Microsoft 0365
            (bool isGetTeamsApiCallSuccess, string teamsResponseData) = DataProvider.GetDataFromInputUrl(token, "/v1.0/me/joinedTeams");

            if (isGetTeamsApiCallSuccess)
            {
                var objTeams = teamsResponseData.AsObject<TeamList>();

                //MyTask:count code needs to be removed 
                int count = 0;

                if (selectedTeams != null && selectedTeams.Count > 0)
                {
                    //Teams which needs to be processed based on user selected values
                    teamsWhichNeedsTobeProcessedBasedOnSelectedDepartment = new List<Team>();
                    foreach (string userSelectedTeam in selectedTeams)
                    {
                        //Adding team names
                        teamsWhichNeedsTobeProcessedBasedOnSelectedDepartment.Add(objTeams.Teams.Where(team => string.Equals(team.DisplayName, userSelectedTeam, StringComparison.OrdinalIgnoreCase)).First());
                    }
                    if (teamsWhichNeedsTobeProcessedBasedOnSelectedDepartment != null && teamsWhichNeedsTobeProcessedBasedOnSelectedDepartment.Count > 0)
                    {
                        //RnD:Added the logic here so that we can optimize using paralled.foreach
                        //Team level processing
                        foreach (var team in teamsWhichNeedsTobeProcessedBasedOnSelectedDepartment)
                        {
                            //Amion data based on microsoft team name 
                            var amionDataTobeProcessedBasedOnTeam = _assignments.FindAll(assignment => string.Equals(assignment.Division, team.DisplayName, StringComparison.OrdinalIgnoreCase));
                            if (amionDataTobeProcessedBasedOnTeam != null && amionDataTobeProcessedBasedOnTeam.Count > 0)
                            {
                                shiftCreationCount = TeamLevelProcessing(team.Id, amionDataTobeProcessedBasedOnTeam, token, MappingFile: mappingFile, StartDate: objAMiONShiftMappingModel.StartDate.AddDays(-1).ToString("yyyy-MM-dd"), EndDate: objAMiONShiftMappingModel.EndDate.AddDays(1).ToString("yyyy-MM-dd"));
                            }
                            else
                            {
                                //No amion data found for the selected team name
                            }
                        }
                    }
                    else
                    {
                        //No team found in microsoft database based on selected values 
                    }
                }
                else
                {
                    //TODO:Need to add current  team value in departmentValWhichNeedsTobeProcessedBasedOnSelectedVal
                }

                #region commented
                //Each data need to push into shift..

                //if (count < 5)
                //{
                //if (mappingFile != null)
                //{
                //    //fetch the team name in microsoft 0365 from mapping file
                //    DataRow amionDivisionEntityTeamRow = Helper.Utilities.CheckValueExistInDatatableBasedonColumnNumber(1, assignment.Division, mappingFile);
                //    assignment.Division = (amionDivisionEntityTeamRow != null && amionDivisionEntityTeamRow.ItemArray.Length > 0) ? amionDivisionEntityTeamRow[2].ToString() : assignment.Division;

                //    //fetch the shift name in microsoft 0365 from mapping file
                //    DataRow amionAssignmentShiftNameRow = Helper.Utilities.CheckValueExistInDatatableBasedonColumnNumber(1, assignment.assignmentName, mappingFile);
                //    assignment.assignmentName = (amionAssignmentShiftNameRow != null && amionAssignmentShiftNameRow.ItemArray.Length > 0) ? amionAssignmentShiftNameRow[2].ToString() : assignment.assignmentName;
                //}

                //Check team id exist with team name
                //if (objTeams != null && objTeams.Teams.Length > 0 && objTeams.Teams.Any(team => team.DisplayName == assignment.Division))
                //{
                //    //check user id exist with user name
                //    //MyTask:Hard Coded emailid logic, needs to be removed and amion data emailid logic needs to be added
                //    (bool isGetUserApiCallSuccess, string userResponseData) = DataProvider.GetDataFromInputUrl(token, @"/v1.0/users/Test7@TESTTESTvsatsinE351Dec21.onmicrosoft.com");
                //    if (isGetUserApiCallSuccess)
                //    {
                //        if ((!string.IsNullOrEmpty(userResponseData)) && (!userResponseData.Contains("error")))
                //        {
                //            //To fetch the team
                //            var team = objTeams.Teams.Single(userTeam => userTeam.DisplayName == assignment.Division);
                //            //check team id exist
                //            if (team != null && (!string.IsNullOrEmpty(team.Id)))
                //            {
                //                //To fetch team scheduling group id
                //                SchedulingGroup schedulingGroup = SchedulingGroupHelper.GetSchedulingGroupsFromTeamIdAndCheckGroupExist(token, team.Id, assignment.assignmentName);
                //                //To fetch the scheduling group id
                //                if (schedulingGroup != null && !string.IsNullOrEmpty(schedulingGroup.Id))
                //                {
                //                    shifts = new Shifts(userResponseData.ParseJson()["id"].ToString(), schedulingGroup.Id, assignment);
                //                }
                //                else
                //                {
                //                    var schedulingGrpId = SchedulingGroupHelper.MapAndCreateSchedulingGrpinTeams(token, team.Id, assignment.assignmentName, new List<Guid> { new Guid(userResponseData.ParseJson()["id"].ToString()) });
                //                    if (!string.IsNullOrEmpty(schedulingGrpId))
                //                    {
                //                        //Check for any error in response, if not parse the data and fetch the user ID and SchedulingGroup id
                //                        shifts = new Shifts(userResponseData.ParseJson()["id"].ToString(), schedulingGrpId, assignment);
                //                    }
                //                    else
                //                    {
                //                        //TODO:Shift group creation failed or errored out with some reason
                //                    }
                //                }
                //                if (shifts != null)
                //                {
                //                    //Push data into shift
                //                    if (CreateShift(team.Id, shifts, token))
                //                    {
                //                        shiftCreationCount++;
                //                    }
                //                    else
                //                    {
                //                        //TODO:Creation failed with some error
                //                    }
                //                }
                //            }
                //            else
                //            {
                //                //TODO:Team id is empty
                //            }
                //        }
                //        else
                //        {
                //            //TODO:Call success but no response data
                //        }
                //    }
                //    else
                //    {
                //        //TODO: Call failed with network error or no data found for the user email id.
                //    }
                //}
                //{
                //    //TODO: If team name not matches the division name or there is no team with the assignment.Division
                //}
                //}
                //else
                //{
                //    //MyTask:Code needs to be removed
                //    //break;
                //}
                //count++;
                //}
                #endregion
            }
            else
            {
                //TODO: If no existing teams found for the user, how this scenario needs to be handled
            }

            return shiftCreationCount;
        }

        /// <summary>
        /// Shift creation helper
        /// </summary>
        /// <param name="teamID">Team id of the microsoft team</param>
        /// <param name="shifts"> shift object to be created</param>
        /// <param name="token">User token of the logged in user</param>
        private static bool CreateShift(string teamID, Shifts shifts, string token)
        {
            //MyTask:Code needs to be removed
            string jsonString = shifts.shift.AsJson().ReadAsStringAsync().GetAwaiter().GetResult();

            var response = RestHelper.PostData(string.Format("{0}/beta/teams/{1}/schedule/shifts", Constant.GraphUrl, teamID), token, shifts.shift.AsJson());

            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                return true;
            }
            else
            {
                //System.IO.File.AppendAllText(@"D:\Anish\Satish documents\Projects\ClientProject\Amion\Import\AmionImport\AMiON.Helper\Testing.json", response.Content.ReadAsStringAsync().GetAwaiter().GetResult());
                //TODO: Log response result is empty
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="teamID"></param>
        /// <param name="amionDataTobeProcessedBasedOnTeam"></param>
        /// <param name="token"></param>
        /// <param name="MappingFile"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public static int TeamLevelProcessing(string teamID, List<AssignmentDetails> amionDataTobeProcessedBasedOnTeam, string token, DataTable MappingFile = null, string StartDate = "", string EndDate = "")
        {
            int shiftCreationCount = 0;
            Shifts shifts = null;


            (bool isGetUserGrpApiSuccess, string userGrpResData) = UserGroupHelper.GetMembersBasedonTeamIdFromGraphApi(teamID, token);

            if (isGetUserGrpApiSuccess)
            {
                var members = userGrpResData.AsObject<MembersList>();

                foreach (var assignment in amionDataTobeProcessedBasedOnTeam)
                {
                    //Mapping teams data from mapping file
                    if (MappingFile != null)
                    {
                        //fetch the team name in microsoft 0365 from mapping file
                        DataRow amionDivisionEntityTeamRow = Helper.Utilities.CheckValueExistInDatatableBasedonColumnNumber(1, assignment.Division, MappingFile);
                        assignment.Division = (amionDivisionEntityTeamRow != null && amionDivisionEntityTeamRow.ItemArray.Length > 0) ? amionDivisionEntityTeamRow[2].ToString() : assignment.Division;

                        //fetch the shift name in microsoft 0365 from mapping file
                        DataRow amionAssignmentShiftNameRow = Helper.Utilities.CheckValueExistInDatatableBasedonColumnNumber(1, assignment.assignmentName, MappingFile);
                        assignment.assignmentName = (amionAssignmentShiftNameRow != null && amionAssignmentShiftNameRow.ItemArray.Length > 0) ? amionAssignmentShiftNameRow[2].ToString() : assignment.assignmentName;
                    }

                    //MyTask: Currently email is not being returned in Amion data hence havent added email id based check
                    string userID = !members.Members.Any(userInTeam => userInTeam.UserPrincipalName == "Test7@TESTTESTvsatsinE351Dec21.onmicrosoft.com") ? "" : members.Members.Where(userInTeam => userInTeam.UserPrincipalName == "Test7@TESTTESTvsatsinE351Dec21.onmicrosoft.com").First().Id;
                    if (!string.IsNullOrEmpty(userID))
                    {
                        //Get all shifts based on team and dates filter
                        var shiftsDataFromGraphApi = ShiftsHelper.GetShifts(teamID, token, StartDate, EndDate);
                        //Check shift exist based on user id
                        if (shiftsDataFromGraphApi != null && shiftsDataFromGraphApi.Shifts != null && shiftsDataFromGraphApi.Shifts.Length > 0 && shiftsDataFromGraphApi.Shifts.Any(shift => shift.UserId == userID))
                        {
                            var responseList = new List<string>();
                            var shiftsBasedOnUserID = shiftsDataFromGraphApi.Shifts.Where(shift => shift.UserId == userID).ToList();
                            foreach (var objShift in shiftsBasedOnUserID)
                            {
                                ShiftsHelper.DeleteShift(teamID, objShift.Id, token);
                            }
                            //Framing batch request for deletion
                            //List<(string guid, string method, string url)> batchDeleteRequest = new List<(string guid, string method, string url)>();
                            //for (int shiftLoop = 0; shiftLoop < shiftsBasedOnUserID.Count; shiftLoop++)
                            //{
                            //    batchDeleteRequest.Add((shiftsBasedOnUserID[shiftLoop].Id, "DELETE", $"/teams/{teamID}/schedule/shifts/{shiftsBasedOnUserID[shiftLoop].Id}"));
                            //}

                            //var combinedBatchRequest = BatchRequestHelper.GenerateJSONBatchRequestList(batchDeleteRequest, 20);

                            ////running all batch request in parallel
                            //Parallel.ForEach(combinedBatchRequest, new ParallelOptions() { MaxDegreeOfParallelism = 10 }, (jsonStringContent) =>
                            //{
                            //    try
                            //    {
                            //        var httpWebResponse = RestHelper.PostData($"{Constant.GraphUrl}/beta/$batch", token, jsonStringContent.AsJson());
                            //        // Parse response
                            //        if (!httpWebResponse.IsSuccessStatusCode)
                            //        {
                            //            responseList.Add(httpWebResponse.Content != null ? httpWebResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult() : string.Empty);
                            //        }
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        //log exception
                            //    }
                            //});
                        }

                        //To fetch team scheduling group id
                        SchedulingGroup schedulingGroup = SchedulingGroupHelper.GetSchedulingGroupsFromTeamIdAndCheckGroupExist(token, teamID, assignment.assignmentName);
                        //To fetch the scheduling group id
                        if (schedulingGroup != null && !string.IsNullOrEmpty(schedulingGroup.Id))
                        {
                            shifts = new Shifts(userID, schedulingGroup.Id, assignment);
                        }
                        else
                        {
                            var schedulingGrpId = SchedulingGroupHelper.MapAndCreateSchedulingGrpinTeams(token, teamID, assignment.assignmentName, new List<Guid> { new Guid(userID) });
                            if (!string.IsNullOrEmpty(schedulingGrpId))
                            {
                                //Check for any error in response, if not parse the data and fetch the user ID and SchedulingGroup id
                                shifts = new Shifts(userID, schedulingGrpId, assignment);
                            }
                            else
                            {
                                //TODO:Shift group creation failed or errored out with some reason
                            }
                        }
                        if (shifts != null)
                        {
                            //Push data into shift
                            if (CreateShift(teamID, shifts, token))
                            {
                                shiftCreationCount++;
                            }
                            else
                            {
                                //TODO:Creation failed with some error
                            }
                        }
                    }
                    else
                    {
                        //TODO:User email not matched in teams response based on amion data
                    }
                }
            }
            else
            {
                //TODO:no members found for the team id
            }
            return 0;
        }
    }
}
