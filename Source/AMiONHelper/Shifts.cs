﻿using System;
using System.Collections.Generic;

namespace AMiON.Helper
{
    public class Shifts
    {
        public Shift shift;
        public Shifts()
        {
        }

        public Shifts(string userID, string schedulingGroupId, AssignmentDetails assignment)
        {
            shift = new Shift
            {
                Id = $"SHFT_{Guid.NewGuid()}",
                UserId = string.IsNullOrEmpty(userID) ? Guid.NewGuid().ToString() : userID,
                SchedulingGroupId = schedulingGroupId,
                SharedShift = new ShiftItem
                {
                    DisplayName = "",
                    StartDateTime = assignment.startDateTime.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    EndDateTime = assignment.endDateTime.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                    Theme = "blue",
                    Activities = new List<ShiftActivity>()
                },
                DraftShift = null
            };
        }
    }
}
