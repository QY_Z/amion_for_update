﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace AMiONGraphShift.ShiftGraphAPI
{
    public partial class ShiftGraphAPIModel
    {
        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("createdDateTime")]
        public DateTimeOffset? CreatedDateTime { get; set; }

        [JsonProperty("lastModifiedDateTime")]
        public DateTimeOffset? LastModifiedDateTime { get; set; }

        [JsonProperty("userId")]
        public Guid UserId { get; set; }
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("schedulingGroupId")]
        public string SchedulingGroupId { get; set; }
        public string SchedulingGroupName { get; set; }

        [JsonProperty("sharedShift")]
        public Shift SharedShift { get; set; }

        [JsonProperty("lastModifiedBy")]
        public LastModifiedBy LastModifiedBy { get; set; }

        [JsonProperty("draftShift")]
        public Shift DraftShift { get; set; }
        public string MobilePhone { get;  set; }
        public string DepartmentName { get;  set; }
        public bool IsActive { get; internal set; }
        public string TeamID { get; internal set; }
        public string EMail { get; internal set; }
    }

    public partial class Shift
    {
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("startDateTime")]
        public DateTimeOffset? StartDateTime { get; set; }

        [JsonProperty("endDateTime")]
        public DateTimeOffset? EndDateTime { get; set; }

        [JsonProperty("theme")]
        public string Theme { get; set; }

        [JsonProperty("activities")]
        public Activity[] Activities { get; set; }
    }

    public partial class Activity
    {
        [JsonProperty("isPaid")]
        public bool IsPaid { get; set; }

        [JsonProperty("startDateTime")]
        public DateTimeOffset StartDateTime { get; set; }

        [JsonProperty("endDateTime")]
        public DateTimeOffset EndDateTime { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("theme")]
        public string Theme { get; set; }
    }

    public partial class LastModifiedBy
    {
        [JsonProperty("application")]
        public object Application { get; set; }

        [JsonProperty("device")]
        public object Device { get; set; }

        [JsonProperty("conversation")]
        public object Conversation { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }
    }

    public partial class User
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
    }


    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }


}