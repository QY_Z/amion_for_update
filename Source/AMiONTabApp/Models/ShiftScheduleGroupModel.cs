﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AMiONGraphShift.Models
{
    public class User
    {

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
    }

    public class LastModifiedBy
    {

        [JsonProperty("application")]
        public object Application { get; set; }

        [JsonProperty("device")]
        public object Device { get; set; }

        [JsonProperty("conversation")]
        public object Conversation { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }
    }

    public class ShiftScheduleGroupModel
    {

        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("createdDateTime")]
        public DateTime? CreatedDateTime { get; set; }

        [JsonProperty("lastModifiedDateTime")]
        public DateTime? LastModifiedDateTime { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("userIds")]
        public IList<string> UserIds { get; set; }

        [JsonProperty("lastModifiedBy")]
        public LastModifiedBy LastModifiedBy { get; set; }
        public string TagId { get; internal set; }
    }

}