﻿using System;
using System.Collections.Generic;

namespace AMiONGraphShift.Models
{
    public class ShiftDetailsFilterModel
    {
        public string SelectedDate { get; set; } = DateTime.Now.ToString("dd MMM, yyyy");
        public string TeamId { get; set; }
        public string TeamName { get; set; }
        public bool IsNext { get; set; }
        public bool IsPagination { get; set; }
    }
}