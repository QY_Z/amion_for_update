﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMiON.ShiftsMapping.Models
{
    public class ShiftMapper
    {
        public string AmionEntityName { get; set; }
        public string AmionEntityValue { get; set; }
        public string ShiftValueToBeMapped { get; set; }
    }
}