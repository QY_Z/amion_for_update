﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AMiONGraphShift.Models
{
    public partial class ShiftTeamModel
    {
        internal string nextLink { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }


        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("isArchived")]
        public bool IsArchived { get; set; }

    }
}