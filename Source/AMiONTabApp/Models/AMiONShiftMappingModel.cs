﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace AMiON.ShiftsMapping.Models
{
    public class AMiONShiftMappingModel
    {
        [Required]
        [Display(Name = "AMiON Login")]
        public string AmionLogin { get; set; }

        //[Required]
        //[Display(Name = "Mapping file")]
        
        //public HttpPostedFileBase AmionMappingFile { get; set; }

        //[Required]
        //[Display(Name = "Start Date")]
        //public string StartDate { get; set; }

        //[Required]
        //[Display(Name = "End Date")]
        //public string EndDate { get; set; }
    }

    public class DepartmentModel
    {
        public string DepartmentName { get; set; }

        public int ShiftsCount { get; set; }

    }
}