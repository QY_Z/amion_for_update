﻿using AMiONGraphShift.ShiftGraphAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMiONGraphShift.Models
{
    public class ShiftDisplayModel
    {
        public IEnumerable<ShiftGraphAPIModel> GraphShiftList { get; set; }
        public ShiftDetailsFilterModel ShiftDetailsFilterModel { get; set; }
    }
}