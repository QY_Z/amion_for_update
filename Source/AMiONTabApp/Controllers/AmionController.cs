﻿using AMiON.Helper;
using AMiON.ShiftsMapping.Models;
using ExcelDataReader;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace AMiON.ShiftsMapping.Controllers
{
    public class AmionController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var cookie = HttpContext.Request.Cookies.Get("amion_");
            var accessToken = cookie?.Value;
            if (string.IsNullOrEmpty(accessToken))
            {
                string baseAddress = HttpContext.Request.Url.GetLeftPart(UriPartial.Authority);
                return Redirect($"{baseAddress}/AmionAuth.html");
            }
            TempData["AuthToken"] = accessToken;
            TempData.Keep("AuthToken");
            return View();
        }


        [HttpGet]
        [Route("AmionData/{amionLogin}")]
        public JsonResult AmionData(string amionLogin)

        {
            var amionData = FetchAmionData(amionLogin);

            if (amionData != null) {
                var uniqueDepartments = amionData.Select(department => department.Division).Distinct();
                List<DepartmentModel> lstDepartmentModel = new List<DepartmentModel>();

                foreach (string departmentName in uniqueDepartments)
                {
                    DepartmentModel departmentModel = new DepartmentModel();
                    departmentModel.DepartmentName = departmentName;
                    departmentModel.ShiftsCount = amionData.FindAll(depart => depart.Division == departmentName).Count;
                    lstDepartmentModel.Add(departmentModel);
                }
                return Json(lstDepartmentModel, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        private static List<AssignmentDetails> FetchAmionData(string loginData)
        {
            if (loginData == null)
            {
                throw new ArgumentNullException(nameof(loginData));
            }
            return Text2Json.JsonDataFromWebExcel(loginData)
                   ?? null;
        }

        [HttpPost]
        [Route("SaveFile")]
        public ActionResult UploadFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        

                        string path = Server.MapPath("~/Uploads/");

                        if(!(Path.GetExtension(path+fname) == ".xlsx" || Path.GetExtension(path+fname) == ".xls"))
                        {
                            return Json(new { message = "Unsupported format of mapping file...", success = false });
                        }

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        if (System.IO.File.Exists(path+fname))
                        {
                            // deletevprevious image
                            System.IO.File.Delete(path+fname);
                        }
                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new { message = "File Uploaded Successfully!", success=true });
                }
                catch (Exception ex)
                {
                    return Json(new { message = "Error occurred.Error details: ", success = false });
                }
            }
            else
            {
                return Json(new { message = "No files selected.", success = false });
            }
        }

        [HttpPost]
        [Route("ProcessShifts/{amionLogin}")]
        public JsonResult ProcessShifts(string amionLogin)
        {
            var token = Helper.Authentication.GetToken();
            var workingThread = new Thread(()=> OperationToCallAsync(token,amionLogin));
            workingThread.Start();

            return Json(new { success = "" });
        }

        void OperationToCallAsync(string token,string amionLoginVal)
        {
            Helper.AMiONShiftMappingModel objAMiONShiftMappingModel = new Helper.AMiONShiftMappingModel();
            objAMiONShiftMappingModel.AmionLogin = amionLoginVal;
            objAMiONShiftMappingModel.StartDate = DateTime.Now;
            objAMiONShiftMappingModel.EndDate = DateTime.Now.AddDays(1);
            ShiftsProcessor.ShiftCreationProcessor(token, objAMiONShiftMappingModel);
        }
    }
}