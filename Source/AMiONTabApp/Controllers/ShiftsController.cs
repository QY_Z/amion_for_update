﻿using AMiONGraphShift.Models;
using AMiONGraphShift.Shared;
using AMiONGraphShift.ShiftGraphAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AMiONGraphShift.Controllers
{
    public class ShiftsController : Controller
    {
        [HttpGet]
        public ActionResult Auth()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Index(string selDate)
        {
            Stream stream = null;
            var date = string.IsNullOrWhiteSpace(selDate) ? DateTime.Now : DateTime.ParseExact(selDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var BaseAdress = $"http://www.amion.com/cgi-bin/ocs?Lo=site&Rpt=625tabs--&Month={date.Month}&Day={date.Day}&Year={date.Year}&Days=1";
            var AMiONShiftDetailList = new List<AMiONShiftDetails>();

            stream = GetAMiONStreamDataFromAPI(BaseAdress);

            DataTable ShiftDetailsDataTable = ConvertStreamDataToDataTable(stream);

            ViewBag.DepartmentList = ShiftDetailsDataTable.AsEnumerable().Select(x => x.Field<string>("Division")).ToList().Distinct();

            AMiONShiftDetailList = ConvertShiftDetailsDataTableToList(ShiftDetailsDataTable);

            return View(AMiONShiftDetailList);
        }

        private List<AMiONShiftDetails> ConvertShiftDetailsDataTableToList(DataTable ShiftDetailsDataTable)
        {
            List<AMiONShiftDetails> AMiONShiftDetailList = new List<AMiONShiftDetails>();

            string GetColumnValueIfExist(DataRow rec, string columnName) => rec.Table.Columns.Contains(columnName) ? rec.Field<string>(columnName) : string.Empty;
            string GetAMiONTimeFormat(string timeString)
            {
                if (string.IsNullOrWhiteSpace(timeString))
                    return string.Empty;
                return DateTime.ParseExact(timeString.PadLeft(4, '0'), "HHmm", CultureInfo.InvariantCulture).ToString("h:m tt").Replace(":0", string.Empty).ToLower();
            }

            foreach (DataRow rec in ShiftDetailsDataTable.AsEnumerable())
            {
                AMiONShiftDetailList.Add(new AMiONShiftDetails()
                {
                    Division = GetColumnValueIfExist(rec, "Division"),
                    Role = GetColumnValueIfExist(rec, "Shift_Name"),
                    Name = GetColumnValueIfExist(rec, "Staff_Name"),
                    ShiftTime = GetAMiONTimeFormat(GetColumnValueIfExist(rec, "Start_Time")) + " - " + GetAMiONTimeFormat(GetColumnValueIfExist(rec, "End_Time")),
                    Pager = GetColumnValueIfExist(rec, "Pager"),
                    Contact = GetColumnValueIfExist(rec, "Contact"),
                    Email = GetColumnValueIfExist(rec, "Email"),
                    ShiftStart = GetColumnValueIfExist(rec, "Start_Time").PadLeft(4, '0'),
                    ShiftEnd = GetColumnValueIfExist(rec, "End_Time").PadLeft(4, '0')
                });
            }

            return AMiONShiftDetailList;
        }

        private static DataTable ConvertStreamDataToDataTable(Stream stream)
        {
            var ShiftDetailsDataTable = new DataTable();
            var isHeader = true;
            using (StreamReader sr = new StreamReader(stream))
            {
                string Row = String.Empty;
                while ((Row = sr.ReadLine()) != null)
                {
                    if (isHeader)
                    {
                        ShiftDetailsDataTable.Columns.AddRange(Row.Split('\t').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => new DataColumn(x, typeof(string))).ToArray());
                        isHeader = false;
                        continue;
                    }
                    ShiftDetailsDataTable.Rows.Add(Row.Split('\t'));
                }
            }

            return ShiftDetailsDataTable;
        }

        private static Stream GetAMiONStreamDataFromAPI(string BaseAdress)
        {
            using (HttpClient client = new HttpClient() { BaseAddress = new Uri(BaseAdress), Timeout = TimeSpan.FromMinutes(10) })
            {
                HttpResponseMessage httpWebResponse = client.SendAsync(new HttpRequestMessage { RequestUri = client.BaseAddress, Method = HttpMethod.Get }).GetAwaiter().GetResult();
                if (httpWebResponse.Content != null)
                {
                    if (httpWebResponse.IsSuccessStatusCode)
                        return httpWebResponse.Content.ReadAsStreamAsync().GetAwaiter().GetResult();

                    Exception exception = new Exception("Fialed to get AMiON data from API");
                    exception.Data.Add("ResponseData", httpWebResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult());
                    throw exception;
                }
            }
            return null;
        }

    }

    public class AMiONShiftDetails
    {
        public string Division { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string ShiftTime { get; set; }

        public string Pager { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string ShiftStart { get; internal set; }
        public string ShiftEnd { get; internal set; }
    }
}