﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMiONGraphShift.Shared
{
    static class AuthHelper
    {
        private static readonly string AuthURL = "https://login.microsoftonline.com/common";
        private static readonly string AuthClientId = "9c9869cf-8e00-45d0-9ee8-fed245fee4c0";

        public static string GetToken()
        {
            var authContext = new AuthenticationContext(AuthURL);
            var result = authContext.AcquireTokenAsync("https://graph.microsoft.com", AuthClientId, new Uri("https://localhost"), new PlatformParameters(PromptBehavior.SelectAccount)).GetAwaiter().GetResult();
            return result.AccessToken;
        }
    }
}